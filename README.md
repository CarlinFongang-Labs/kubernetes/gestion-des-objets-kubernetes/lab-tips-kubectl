# Tips : Usage de kubectl

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______ 

# Objectif
Ce lab illustre les techniques facilitant l'usage de kubectl.


# Commandes impératives

Création d'un deployment de façon impérative 

```bash
kubectl create deployment my-deployment --image=nginx
```

>![Alt text](img/image.png)
*Création du deployment*


# Obtenir des exemples de YAML : 

Utilisation de **kubectl create deployment my-deployment --image=nginx --dry-run -o yaml** pour obtenir un fichier YAML de base.

```bash
kubectl create deployment my-deployment --image=nginx --dry-run -o yaml
```

>![Alt text](img/image-1.png)
*Génération du code YAML*

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: my-deployment
  name: my-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: my-deployment
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: my-deployment
    spec:
      containers:
      - image: nginx
        name: nginx
        resources: {}
status: {}
```

La commande à aisni permis de générer un manisfest pour un usage déclaratif


# Exporter du contenu YAML vers un fichier

L'idée est de rediriger la sortie vers un fichier avec : 

```bash
kubectl create deployment my-deployment --image=nginx --dry-run -o yaml > my-deployment.yml
```
>![Alt text](img/image-2.png)
*Exportation du manifest réussie*

# Créer ou appliquer un objet

Nous pouvons au besoin modifier le fichier YAML, puis créer l'objet avec **kubectl create -f my-deployment.yml** ou **kubectl apply -f my-deployment.yml**.

```bash
kubectl create -f my-deployment.yml
```

>![Alt text](img/image-3.png)
*Il nous est indiqué que le deployment existe déjà*

Ainsi nous allons juste procéder à une mise à jour 

```bash
kubectl apply -f my-deployment.yml
```

>![Alt text](img/image-4.png)
*Mise à jour de deployment*

# Enregistrer une commande

Nous allons faire évoluer notre deployment en le faisant passer d'un replicas à 5.

```bash
kubectl scale deployment my-deployment --replicas=5 --record 
```

>![Alt text](img/image-5.png)
*Scale up du deployment*

Vérification de l'enregistrement de l'annotation

>![Alt text](img/image-6.png)
*Commentaire sur la ligne Annotations*

Un commantaire relatif au changement et à la commande utilisée à été rajoutée.

On remarque tout de même que cette commande est déprécié par les version recente de Kubernetes, comme alternative, nous pouvons utiliser : 

```bash 
kubectl annotate deployment my-deployment kubernetes.io/change-cause="scaled to 5 replicas" --overwrite
kubectl scale deployment my-deployment --replicas=3
```

>![Alt text](img/image-7.png)

en vérifiant de nouveau la description du deployment : 

```bash
kubectl describe deploy my-deployment
```

>![Alt text](img/image-8.png)
*Mise à jour des annotations*

# Utiliser la documentation

Copier un exemple YAML depuis la documentation et l'utiliser pour créer un objet.

1. Créer un fichier yaml vide 

```bash
nano doc-deployment.yml
```
2. Accéder à un template de deployment

[kubernetes > Deployment docs](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#creating-a-deployment)

`"controllers/nginx-deployment.yaml"`

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
```

3. Exécution du deployment

Une fois le fichier mis à jour, l'on enregistre et exécute le deployment 

```bash
kubectl apply -f doc-deployment.yml
```

>![Alt text](img/image-9.png)
*Deployment réussi*

Vérification 

```bash
kubectl get deployment
```

>![Alt text](img/image-10.png)
*l'objet nginx-deployment en cours d'exécution*